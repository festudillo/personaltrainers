﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using personal_trainers.Repository;
using personal_trainers.Repository.Models;

namespace personal_trainers.Business
{
    public class UserManager : BaseManager, IUserManager
    {
        #region Attributes
        private readonly IUserRepository _repository;
        private readonly ITagRepository _tagRepository;
        #endregion

        #region Constructors
        public UserManager(DbContext context) : base(context)
        {
            this._repository = new UserRepository(this.UnitOfWork);
            this._tagRepository = new TagRepository(this.UnitOfWork);
        }

        #endregion

        #region Public Methods

        public bool SaveUser(User user)
        {
            bool result;
            try
            {
                if (_repository != null)
                {
                    var idRepo = _repository.All().Any(p => p.Id == user.Id);
                    if (!idRepo)
                    {
                        _repository.Add(user);
                    }
                    else
                    {
                        UpdateUser(user);
                    }
                    _repository.UnitOfWork.Commit();
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)//TO DO: Create logger with log4net
            {
                throw new Exception("Error saving category", ex);
            }
            return result;
        }


        public IQueryable<User> GetUsers()
        {
            return UnitOfWork != null ? _repository.All() : null;
        }

        public User GetUser(int userId)
        {
            var result = new User();
            try
            {
                result = _repository.Get(userId);
            }
            catch (Exception ex)//TO DO: Create logger with log4net
            {
                throw new Exception("Error saving category", ex);
            }
            return result;
        }

        public void DeleteCategory(int Id)
        {
            try
            {
                var deleteItem = _repository.Get(Id);
                _repository.Delete(deleteItem);
                _repository.UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw new Exception("Error deleting category", ex);
            }
        }
        #endregion

        #region Private Methods

        private void UpdateUser(User user)
        {
            var userDb = _repository.Get(user.Id);
            userDb.FirstName = user.FirstName;
            userDb.Catchword = user.Catchword;
            userDb.DateOfBirth = user.DateOfBirth;
            userDb.Email = user.Email;
            userDb.ImageUrl = user.ImageUrl;
            userDb.LastName = user.LastName;
            userDb.Telephone = user.Telephone;
            userDb.UserName = user.UserName;
            userDb.Addresses = user.Addresses;
            if (!userDb.Tags.Any(p => user.Tags.Any(c => c.Id == p.Id)))
            {
                userDb.Tags.Clear();
                if (user.Tags != null)
                    foreach (var tag in user.Tags)
                    {
                        userDb.Tags.Add(_tagRepository.Get(tag.Id));
                    }
            }
            userDb.Experiences = user.Experiences;
        }
        #endregion
    }
}
