﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using personal_trainers.Repository;

namespace personal_trainers.Business
{
    public class BaseManager
    {
        protected readonly IUnitOfWork UnitOfWork;
        private readonly DbContext _context;

        public BaseManager(DbContext context)
        {
            this._context = context;
            this.UnitOfWork = new EFUnitOfWork(_context);
        }
    }
}
