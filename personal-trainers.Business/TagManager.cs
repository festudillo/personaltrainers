﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using personal_trainers.Repository;
using personal_trainers.Repository.Models;

namespace personal_trainers.Business
{
    public interface ITagManager
    {
        bool SaveTag(Tag Tag);
        IQueryable<Tag> GetTags();
        Tag GetTag(int TagId);
        void DeleteCategory(int Id);
    }

    public class TagManager : BaseManager, ITagManager
    {
        #region Attributes
        private readonly ITagRepository _repository;
        #endregion

        #region Constructors
        public TagManager(DbContext context) : base(context)
        {
            this._repository = new TagRepository(this.UnitOfWork);
        }

        #endregion

        #region Public Methods

        public bool SaveTag(Tag Tag)
        {
            bool result;
            try
            {
                if (_repository != null)
                {
                    var idRepo = _repository.All().Select(p => p.Id);
                    if (!idRepo.Contains(Tag.Id))
                    {
                        _repository.Add(Tag);
                    }
                    else
                    {
                        UpdateTag(Tag);
                    }
                    _repository.UnitOfWork.Commit();
                    result = true;
                }
                else
                {
                    result = false;
                }
            }
            catch (Exception ex)//TO DO: Create logger with log4net
            {
                throw new Exception("Error saving category", ex);
            }
            return result;
        }


        public IQueryable<Tag> GetTags()
        {
            return UnitOfWork != null ? _repository.All() : null;
        }

        public Tag GetTag(int TagId)
        {
            var result = new Tag();
            try
            {
                result = _repository.Get(TagId);
            }
            catch (Exception ex)//TO DO: Create logger with log4net
            {
                throw new Exception("Error saving category", ex);
            }
            return result;
        }

        public void DeleteCategory(int Id)
        {
            try
            {
                var deleteItem = _repository.Get(Id);
                _repository.Delete(deleteItem);
                _repository.UnitOfWork.Commit();
            }
            catch (Exception ex)
            {
                throw new Exception("Error deleting category", ex);
            }
        }
        #endregion

        #region Private Methods

        private void UpdateTag(Tag Tag)
        {
            _repository.Get(Tag.Id).Description = Tag.Description;
        }
        #endregion
    }
}
