﻿using System.Linq;
using personal_trainers.Repository.Models;

namespace personal_trainers.Business
{
    public interface IUserManager
    {
        bool SaveUser(User user);
        IQueryable<User> GetUsers();
        User GetUser(int userId);
        void DeleteCategory(int Id);
    }
}