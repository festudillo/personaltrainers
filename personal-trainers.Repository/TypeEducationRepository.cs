using System;
using System.Linq;
using System.Collections.Generic;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{   
    public  partial class TypeEducationRepository : EFRepository<TypeEducation>, ITypeEducationRepository
    {
        public TypeEducationRepository(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public TypeEducation Get(int Id) 
        {
            return this.FirstOrDefault(e => e.Id == Id);
        }
    }

    public  partial interface ITypeEducationRepository : IRepository<TypeEducation>
    {
        TypeEducation Get(int Id);
    }
}