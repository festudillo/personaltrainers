using System;
using System.Linq;
using System.Collections.Generic;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{   
    public  partial class EducationRepository : EFRepository<Education>, IEducationRepository
    {
        public EducationRepository(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public Education Get(int Id) 
        {
            return this.FirstOrDefault(e => e.Id == Id);
        }
    }

    public  partial interface IEducationRepository : IRepository<Education>
    {
        Education Get(int Id);
    }
}