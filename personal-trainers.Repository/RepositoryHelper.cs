
namespace personal_trainers.Repository
{
    public static class RepositoryHelper
    {
		public static IUnitOfWork UnitOfWork { get; set; }
		
        
		public static IAddressRepository AddressRepository
		{
			get
			{
				return new AddressRepository(UnitOfWork);
			}
		}

        public static AddressRepository GetAddressRepository(IUnitOfWork unitOfWork)
        {
            var repository = new AddressRepository(unitOfWork);
            //repository.UnitOfWork = unitOfWork;
            return repository;
        }		

		public static IEducationRepository EducationRepository
		{
			get
			{
				return new EducationRepository(UnitOfWork);
			}
		}

        public static EducationRepository GetEducationRepository(IUnitOfWork unitOfWork)
        {
            var repository = new EducationRepository(unitOfWork);
            //repository.UnitOfWork = unitOfWork;
            return repository;
        }		

		public static IExperienceRepository ExperienceRepository
		{
			get
			{
				return new ExperienceRepository(UnitOfWork);
			}
		}

        public static ExperienceRepository GetExperienceRepository(IUnitOfWork unitOfWork)
        {
            var repository = new ExperienceRepository(unitOfWork);
            //repository.UnitOfWork = unitOfWork;
            return repository;
        }		

		public static ITagRepository TagRepository
		{
			get
			{
				return new TagRepository(UnitOfWork);
			}
		}

        public static TagRepository GetTagRepository(IUnitOfWork unitOfWork)
        {
            var repository = new TagRepository(unitOfWork);
            //repository.UnitOfWork = unitOfWork;
            return repository;
        }		

		public static ITypeEducationRepository TypeEducationRepository
		{
			get
			{
				return new TypeEducationRepository(UnitOfWork);
			}
		}

        public static TypeEducationRepository GetTypeEducationRepository(IUnitOfWork unitOfWork)
        {
            var repository = new TypeEducationRepository(unitOfWork);
            //repository.UnitOfWork = unitOfWork;
            return repository;
        }		

		public static IUserRepository UserRepository
		{
			get
			{
				return new UserRepository(UnitOfWork);
			}
		}

        public static UserRepository GetUserRepository(IUnitOfWork unitOfWork)
        {
            var repository = new UserRepository(unitOfWork);
            //repository.UnitOfWork = unitOfWork;
            return repository;
        }		
    }
}


