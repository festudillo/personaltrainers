using System.Data.Entity;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{
    public class EFUnitOfWork : IUnitOfWork
    {
        public DbContext Context { get; set; }

        public EFUnitOfWork(DbContext context)
        {
            Context = context;//new Entities();
			RepositoryHelper.UnitOfWork = this;
        }

        public void Commit()
        {
            (Context as DbContext).SaveChanges();
        }
        
        public bool LazyLoadingEnabled
        {
            get { return (Context as DbContext).Configuration.LazyLoadingEnabled; }
            set { (Context as DbContext).Configuration.LazyLoadingEnabled = value; }
        }

        public bool ProxyCreationEnabled
        {
            get { return (Context as DbContext).Configuration.ProxyCreationEnabled; }
            set { (Context as DbContext).Configuration.ProxyCreationEnabled = value; }
        }
        
        public string ConnectionString
        {
            get { return (Context as DbContext).Database.Connection.ConnectionString; }
            set { (Context as DbContext).Database.Connection.ConnectionString = value; }
        }
    }
}
