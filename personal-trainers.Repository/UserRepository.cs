using System;
using System.Linq;
using System.Collections.Generic;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{   
    public  partial class UserRepository : EFRepository<User>, IUserRepository
    {
        public UserRepository(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public User Get(int Id) 
        {
            return this.FirstOrDefault(e => e.Id == Id);
        }
    }

    public  partial interface IUserRepository : IRepository<User>
    {
        User Get(int Id);
    }
}