using System;
using System.Linq;
using System.Collections.Generic;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{   
    public  partial class ExperienceRepository : EFRepository<Experience>, IExperienceRepository
    {
        public ExperienceRepository(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public Experience Get(int Id) 
        {
            return this.FirstOrDefault(e => e.Id == Id);
        }
    }

    public  partial interface IExperienceRepository : IRepository<Experience>
    {
        Experience Get(int Id);
    }
}