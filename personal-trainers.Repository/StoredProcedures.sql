IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetAddress]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[GetAddress]
END
GO
CREATE PROCEDURE [dbo].[GetAddress]
(
        @Id int = NULL,
        @idioma VARCHAR(2) = NULL
)
AS
    BEGIN
        SELECT 
        				Addresses.Id,
				Addresses.Street,
				Addresses.Village,
				Addresses.City,
				Addresses.PostalCode,
				Addresses.Users
        FROM Addresses
        WHERE 
                ( @Id is NULL OR Addresses.Id = @Id)
    END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetEducation]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[GetEducation]
END
GO
CREATE PROCEDURE [dbo].[GetEducation]
(
        @Id int = NULL,
        @idioma VARCHAR(2) = NULL
)
AS
    BEGIN
        SELECT 
        				Educations.Id,
				Educations.TypeEducationId,
				Educations.Name,
				Educations.TypeEducation,
				Educations.Users
        FROM Educations
        WHERE 
                ( @Id is NULL OR Educations.Id = @Id)
    END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetExperience]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[GetExperience]
END
GO
CREATE PROCEDURE [dbo].[GetExperience]
(
        @Id int = NULL,
        @idioma VARCHAR(2) = NULL
)
AS
    BEGIN
        SELECT 
        				Experiences.Id,
				Experiences.Position,
				Experiences.StartDate,
				Experiences.EndDate,
				Experiences.Place,
				Experiences.Users
        FROM Experiences
        WHERE 
                ( @Id is NULL OR Experiences.Id = @Id)
    END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTag]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[GetTag]
END
GO
CREATE PROCEDURE [dbo].[GetTag]
(
        @Id int = NULL,
        @idioma VARCHAR(2) = NULL
)
AS
    BEGIN
        SELECT 
        				Tags.Id,
				Tags.Description,
				Tags.Users
        FROM Tags
        WHERE 
                ( @Id is NULL OR Tags.Id = @Id)
    END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetTypeEducation]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[GetTypeEducation]
END
GO
CREATE PROCEDURE [dbo].[GetTypeEducation]
(
        @Id int = NULL,
        @idioma VARCHAR(2) = NULL
)
AS
    BEGIN
        SELECT 
        				TypeEducations.Id,
				TypeEducations.Description,
				TypeEducations.IconPath,
				TypeEducations.Educations
        FROM TypeEducations
        WHERE 
                ( @Id is NULL OR TypeEducations.Id = @Id)
    END
GO

IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetUser]') AND type in (N'P', N'PC'))
BEGIN
    DROP PROCEDURE [dbo].[GetUser]
END
GO
CREATE PROCEDURE [dbo].[GetUser]
(
        @Id int = NULL,
        @idioma VARCHAR(2) = NULL
)
AS
    BEGIN
        SELECT 
        				Users.Id,
				Users.FirstName,
				Users.LastName,
				Users.Telephone,
				Users.Email,
				Users.UserName,
				Users.ImageUrl,
				Users.AddressId,
				Users.DateOfBirth,
				Users.Catchword,
				Users.Addresses,
				Users.Educations,
				Users.Experiences,
				Users.Tags
        FROM Users
        WHERE 
                ( @Id is NULL OR Users.Id = @Id)
    END
GO

