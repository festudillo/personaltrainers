using System.Data.Entity;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{
    public interface IUnitOfWork
    {
        DbContext Context { get; set; }
        void Commit();
        bool LazyLoadingEnabled { get; set; }
        bool ProxyCreationEnabled { get; set; }
        string ConnectionString { get; set; }
    }
}