//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace personal_trainers.Repository.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class User
    {
        public User()
        {
            this.Addresses = new HashSet<Address>();
            this.Educations = new HashSet<Education>();
            this.Experiences = new HashSet<Experience>();
            this.Tags = new HashSet<Tag>();
        }
    
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public string ImageUrl { get; set; }
        public Nullable<int> AddressId { get; set; }
        public Nullable<System.DateTime> DateOfBirth { get; set; }
        public string Catchword { get; set; }
    
        public virtual ICollection<Address> Addresses { get; set; }
        public virtual ICollection<Education> Educations { get; set; }
        public virtual ICollection<Experience> Experiences { get; set; }
        public virtual ICollection<Tag> Tags { get; set; }
    }
}
