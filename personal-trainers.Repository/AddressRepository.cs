using System;
using System.Linq;
using System.Collections.Generic;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{   
    public  partial class AddressRepository : EFRepository<Address>, IAddressRepository
    {
        public AddressRepository(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public Address Get(int Id) 
        {
            return this.FirstOrDefault(e => e.Id == Id);
        }
    }

    public  partial interface IAddressRepository : IRepository<Address>
    {
        Address Get(int Id);
    }
}