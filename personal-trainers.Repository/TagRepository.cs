using System;
using System.Linq;
using System.Collections.Generic;
using personal_trainers.Repository.Models;

namespace personal_trainers.Repository
{   
    public  partial class TagRepository : EFRepository<Tag>, ITagRepository
    {
        public TagRepository(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public Tag Get(int Id) 
        {
            return this.FirstOrDefault(e => e.Id == Id);
        }
    }

    public  partial interface ITagRepository : IRepository<Tag>
    {
        Tag Get(int Id);
    }
}