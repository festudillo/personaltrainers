﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using DataAnnotationsExtensions;

namespace personal_trainers.ViewModels
{
    public class ExperienceViewModel
    {
        public int Id { get; set; }

        [Required]
        [StringLength(150)]
        public string Position { get; set; }

        [Date]
        [Display(Name = "Start Date")]
        [Required]
        public DateTime? StartDate { get; set; }
                
        [Date]
        [Display(Name = "End Date")]
        [Required]
        public DateTime? EndDate { get; set; }

        [Required]
        [StringLength(50)]
        public string Place { get; set; }
    }
}
