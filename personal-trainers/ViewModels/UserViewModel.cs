﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DataAnnotationsExtensions;
using personal_trainers.Models;
using personal_trainers.Repository.Models;

namespace personal_trainers.ViewModels
{
    public class UserViewModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Username")]
        public string UserName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [System.ComponentModel.DataAnnotations.Url]
        [Display(Name = "Image")]
        public string ImageUrl { get; set; }
        public string Telephone { get; set; }

        [Required]
        [StringLength(50)]
        [Email]
        public string Email { get; set; }

        [Date]
        [Display(Name = "Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }

        [StringLength(150)]
        [Display(Name = "Catchword")]
        public string Catchword { get; set; }

        public IEnumerable<int> TagsId { get; set; }

        public ICollection<TagViewModel> Tags { get; set; }
        public ICollection<Address> Addresses { get; set; }
        public ICollection<Education> Educations { get; set; }
        public ICollection<Experience> Experiences { get; set; }

        public LocalPasswordModel PasswordModel { get; set; }

        public static UserViewModel ConvertToViewModel(User user)
        {
            return new UserViewModel()
                {
                    Id = user.Id,
                    Addresses = user.Addresses,
                    Tags = TagViewModel.ConvertListToViewModel(user.Tags).ToList(),
                    Catchword = user.Catchword,
                    DateOfBirth = user.DateOfBirth,
                    Educations = user.Educations,
                    Email = user.Email,
                    Experiences = user.Experiences,
                    FirstName = user.FirstName,
                    UserName = user.UserName,
                    ImageUrl = user.ImageUrl,
                    LastName = user.LastName,
                    TagsId = user.Tags.Select(p=>p.Id)
                };
        }

        public static User ConvertToModel(UserViewModel user)
        {
            user.Tags = new List<TagViewModel>();
            if (user.TagsId != null)
                foreach (var tagId in user.TagsId)
                {
                    user.Tags.Add(new TagViewModel() { Id = tagId });
                }
            return new User()
            {
                Id = user.Id,
                Addresses = user.Addresses,
                Tags = TagViewModel.ConvertListToModel(user.Tags),
                Catchword = user.Catchword,
                DateOfBirth = user.DateOfBirth,
                Educations = user.Educations,
                Email = user.Email,
                Experiences = user.Experiences,
                FirstName = user.FirstName,
                UserName = user.UserName,
                ImageUrl = user.ImageUrl,
                LastName = user.LastName
            };
        }
    }
}