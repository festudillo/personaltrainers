﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using personal_trainers.Repository.Models;

namespace personal_trainers.ViewModels
{
    public class TagViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }

        public static TagViewModel ConvertToViewModel(Tag tag)
        {
            return new TagViewModel()
                {
                    Id = tag.Id,
                    Description = tag.Description
                };
        }

        public static IEnumerable<TagViewModel> ConvertListToViewModel(ICollection<Tag> tags)
        {
            return tags.Select(ConvertToViewModel);
        }

        public static ICollection<Tag> ConvertListToModel(IEnumerable<TagViewModel> tags)
        {
            return tags.Select(p=> new Tag(){Id = p.Id, Description = p.Description}).ToList();
        }
    }
}
