﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using personal_trainers.Models;

namespace personal_trainers.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<UsersContext>(null);

                try
                {
                    using (var context = new UsersContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    SeedMembership();
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }

            private static void SeedMembership()
            {
                if (!WebSecurity.Initialized)
                {
                    WebSecurity.InitializeDatabaseConnection("ConnStringForWebSecurity", "tblUsers", "Id", "UserName",
                                                             autoCreateTables: true);
                }
                var roles = (SimpleRoleProvider)Roles.Provider;
                var membership = (SimpleMembershipProvider)Membership.Provider;

                if (!roles.RoleExists("Admin"))
                {
                    roles.CreateRole("Admin");
                }
                if (!roles.RoleExists("pt"))
                {
                    roles.CreateRole("pt");
                }
                if (!roles.RoleExists("pu"))
                {
                    roles.CreateRole("pu");
                }
                if (membership.GetUser("admin", false) == null)
                {
                    membership.CreateUserAndAccount("admin", "admin123admin");
                }
                if (membership.GetUser("pt", false) == null)
                {
                    membership.CreateUserAndAccount("pt", "admin123admin");
                }
                if (membership.GetUser("pu", false) == null)
                {
                    membership.CreateUserAndAccount("pu", "admin123admin");
                }
                if (!roles.GetRolesForUser("admin").Contains("Admin"))
                {
                    roles.AddUsersToRoles(new[] { "admin" }, new[] { "Admin" });
                }
                if (!roles.GetRolesForUser("pt").Contains("pt"))
                {
                    roles.AddUsersToRoles(new[] { "pt" }, new[] { "pt" });
                }
                if (!roles.GetRolesForUser("pu").Contains("pu"))
                {
                    roles.AddUsersToRoles(new[] { "pu" }, new[] { "pu" });
                }
            }
        }
    }
}
