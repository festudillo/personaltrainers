﻿using System.Web.Mvc;
using personal_trainers.Filters;

namespace personal_trainers.Controllers
{
    [InitializeSimpleMembership]
    public class BaseController : Controller
    {
    }
}
