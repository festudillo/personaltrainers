﻿ using System;
using System.Collections.Generic;
 using System.Data.Entity;
 using System.Linq;
using System.Web;
using System.Web.Mvc;
 using personal_trainers.Repository;

namespace personal_trainers.Controllers
{
    public class HomeController : BaseController
    {

        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
