﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using personal_trainers.Business;
using personal_trainers.Repository;
using personal_trainers.Repository.Models;

namespace personal_trainers.Helpers
{
    public class AuthenticationHelper
    {
        private static IUserManager _userManager;
        public AuthenticationHelper(IUserManager userManager)
        {
            _userManager = userManager;
        }
        public static bool IsUserInRole(string userName, string role)
        {
            InitializeDatabaseConn();
            return Roles.Provider.IsUserInRole(username: userName, roleName:role);
        }

        private User _currentUser;
        public User CurrentUser
        {
            get
            {
                InitializeDatabaseConn();
                if (_currentUser == null && WebSecurity.IsAuthenticated)
                {
                    _currentUser = _userManager.GetUser(WebSecurity.CurrentUserId);
                }
                return _currentUser;
            }
        }

        private static void InitializeDatabaseConn()
        {
            if (!WebSecurity.Initialized)
            {
                WebSecurity.InitializeDatabaseConnection("ConnStringForWebSecurity", "tblUsers", "Id", "UserName",
                                                     autoCreateTables: true);
            }
        }
    }
}